#!/bin/bash

# Directories to check
sysdir="/usr/share/games/NZP"
configdir="/home/$USER/.config/NZP"
binname=nzportable
image_path="/usr/share/games/NZP/nzp-title.png"
icon_path="/usr/share/games/$Game/nzp.svg"

sleep 3 
if [ -d "$configdir" ]; then
  ### Take action if $configdir exists ###
  echo "Game files exists, skipping and checking game files before launching game.."
  
(
    echo "10" ; sleep 1
    echo "# Checking for updates" ; sleep 1    
 echo "# Checking for updates" ; sleep 1    
# Specify the two files to compare
    file1="/usr/share/games/NZP/version.txt"
    file2="/home/$USER/.config/NZP/version.txt"
    
# Check if both files are provided
if [[ -z "$file1" || -z "$file2" ]]; then
    echo "Usage: $0 <file1> <file2>"
    exit 1
fi

# Compare the contents of the two files
if cmp -s "$file1" "$file2"; then
    echo "50" ; sleep 1
    echo "# Game files up to date" ; sleep 1 
else
    echo "50" ; sleep 1
    echo "# Updating game files.." ; sleep 1  
    cp -r /usr/share/games/NZP/version.txt /home/$USER/.config/NZP
    unzip -o /usr/share/games/NZP/nzportable-*.zip -d /home/$USER/.config/NZP
    chmod +x /home/$USER/.config/NZP/nzportable
fi
    echo "99" ; sleep 1
    echo "# Launching game" ; sleep 1
    echo "100" ; sleep 1
) |
yad --progress \
  --title="Checking $Game files.." \
  --text="" \
  --window-icon="$icon_path" \
  --image="$image_path" \
  --percentage=0 \
  --auto-close

if [ "$?" = -1 ] ; then
        yad --error \
          --text="Check failed."
fi
# End of update and launch portion of the script
else
# Start of setup portion of the script
###  Control will jump here if $configdir does NOT exists ###
  echo "Welcome to the $Game setup, we will begin setting the game up for you."
  yad --width=840 --height=100 --info --title="$Game Installation" --window-icon="$icon_path" --image="$image_path" --text="Welcome to the $Game setup, we will begin setting the game up for you."  --button="OK:1" --button="Cancel:0"
  if [ $? -eq 0 ]; then
  echo "Script exited by user"
  exit 0
fi
(
echo "10" ; sleep 1
echo "# Setting game files" ; sleep 1
echo "50" ; sleep 1
echo "# Creating config folder" ; sleep 1
mkdir $configdir
echo "85" ; sleep 1
echo "# Uncompressing game files" ; sleep 1
unzip -o /usr/share/games/NZP/nzportable-*.zip -d /home/$USER/.config/NZP
echo "90" ; sleep 1
echo "# Setting binary as executable" ; sleep 1
chmod +x /home/$USER/.config/NZP/nzportable
echo "95" ; sleep 1
echo "# copying file" ; sleep 1
cp -r /usr/share/games/NZP/version.txt /home/$USER/.config/NZP
echo "100" ; sleep 1
) |
yad --progress \
  --width=550 \
  --height=100 \
  --title="Setting up $Game" \
  --text="Preparing to setup game..." \
  --window-icon="$icon_path" \
  --percentage=0 \
  --auto-close

if [ "$?" = -1 ] ; then
        yad --error \
          --text="installation failed."
fi
fi

echo "Starting game"
cd $configdir
./$binname
